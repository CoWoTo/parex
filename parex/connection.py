from paramiko import SSHClient, AutoAddPolicy
from .session import Session


class Client:

    def __init__(self):
        self.store = Session()

    def _verify_con(self, alias):
        connection = self.store.get(alias)
        if connection.get_transport().is_active():
            return connection
        else:
            self.store.remove(alias)
            self.setup(alias)
            return self.store.get(alias)

    def setup(self, alias):
        connection = SSHClient()
        connection.set_missing_host_key_policy(AutoAddPolicy())
        connection.load_system_host_keys()
        self.store.save(connection, alias)

    def teardown(self, alias):
        connection = self.store.get(alias)
        connection.close()
        self.store.remove(alias)

    def connect_to(self, alias, **node):
        """
        Method connects to node
        :param dict node: you can put
            hostname,
            port=SSH_PORT,
            username=None,
            password=None,
            pkey=None,
            key_filename=None,
            timeout=None,
            allow_agent=True,
            look_for_keys=True,
            compress=False,
            sock=None,
            gss_auth=False,
            gss_kex=False,
            gss_deleg_creds=True,
            gss_host=None,
            banner_timeout=None,
            auth_timeout=None,
            gss_trust_dns=True,
            passphrase=None
        :param str alias:
        :return: None
        """
        connection = self.store.get(alias)
        connection.connect(**node)

    def send_command(self, command, alias):
        """
        Method sends command via ssh and returns output
        :param str command: for ex "ls -la"
        :param str alias: some sting like: connection1
        :return: str (stdin, stdout, stderr)
        """
        connection = self._verify_con(alias)
        stdin, stdout, stderr = connection.exec_command(command)
        return stdout.readlines(), stdout.channel.recv_exit_status()
