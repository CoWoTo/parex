class AbstractSession:
    """
    Abstract singleton for session handler
    """
    _instance = None
    _sessions = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def save(self, obj, alias):
        pass

    def get(self, alias):
        pass

    def remove(self, alias):
        pass

    def clear(self):
        pass


class Session(AbstractSession):
    _sessions = dict()

    def _check_buffer(self, alias):
        return alias in self._sessions.keys()

    def save(self, obj, alias):
        if self._check_buffer(alias) is False:
            self._sessions[alias] = obj
        else:
            raise Exception("Alias {} already taken !".format(alias))

    def get(self, alias):
        if self._check_buffer(alias):
            return self._sessions.get(alias)
        else:
            raise Exception("There is no such alias as {}".format(alias))

    def remove(self, alias):
        if self._check_buffer(alias):
            self._sessions.pop(alias)
        else:
            raise Exception("There is no such alias as {}".format(alias))

    def clear(self):
        self._sessions.clear()
