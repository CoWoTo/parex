"""
author: Konrad Jazownik
contact: jazk@viessmann.com
"""
from setuptools import setup

setup(name='parex',
      version='1.0',
      description='open source interface for ssh connections',
      author='Konrad Jazownik',
      author_email='konjaz@gmail.com',
      install_requires=['paramiko'],
      packages=['parex']
      )
